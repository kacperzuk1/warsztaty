var fs = require( 'fs' ),
    html5Lint = require( 'html5-lint' );

fs.readFile( 'src/index.html', 'utf8', function( err, html ) {
  if ( err )
    throw err;

  html5Lint( html, function( err, results ) {
    results.messages.forEach( function( msg ) {
      var type = msg.type, // error or warning
          message = msg.message;

      console.log( "HTML5 Lint [%s]: %s", type, message );
    });
    if(results.messages.length == 0) {
        console.log("No errors :D");
        process.exit(0);
    } else {
        console.log("You didn't pass the test :(");
        process.exit(1);
    }
  });
});

